import Vue from 'vue'
import App from './App.vue'

//引入安裝註冊bootstrap-vue
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)

//引入安裝註冊路由
import VueRouter from 'vue-router';
Vue.use(VueRouter);
import routes from './router.config';
const router=new VueRouter(routes)

//引入狀態管理  自動找底下index
import store from './store'

import animated from 'animate.css' 
Vue.use(animated)


import waypoints from 'waypoints/lib/noframework.waypoints.min.js';
Vue.prototype.waypoints= waypoints

//引入安裝取得url的參數
import animeToolPlugin from './plugins/animeTool';
Vue.use(animeToolPlugin); 

//引入axios
import axios from 'axios'
Vue.prototype.axios= axios

import axiosPlugin from '@/plugins/axios';
Vue.use(axiosPlugin);

//安裝VUEX

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
