/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */

import Vue from 'vue'


import {
  QUERY_COMPANYGOAL_INFO,
  QUERY_BANNER_INFO
} from './mutationTypes';

const actions = {
  /**
   * 取得公司目標
   */
  queryCompanyGoal({ commit }) {
    Vue.prototype.$axios({
      url:'data/companyGoal.json',
      params:{}
    }).then((resp) => { // 回應正常
      const respData = resp.data;
      commit(QUERY_COMPANYGOAL_INFO, {
        data: respData,
      });
    }).catch((error) => { // 異常處理
      window.console.error(error);
    });
  },
  /**
   * 取得banner
   */
  queryBanner({ commit }) {
    Vue.prototype.$axios({
      url:'data/banner.json',
      params:{}
    }).then((resp) => { // 回應正常
      const respData = resp.data;
      commit(QUERY_BANNER_INFO, {
        data: respData,
      });
    }).catch((error) => { // 異常處理
      window.console.error(error);
    });
  },
};

export default actions;
