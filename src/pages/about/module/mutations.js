import {
  QUERY_BANNER_INFO,
  QUERY_COMPANYGOAL_INFO,
} from './mutationTypes';

const mutations = {
  [QUERY_BANNER_INFO](state, payload) { // 取得公司目標資料
    state.bannerData = payload;
  },
  [QUERY_COMPANYGOAL_INFO](state, payload) { // 取得banner資料
    state.companyGoalData = payload;
  },
};

export default mutations;
