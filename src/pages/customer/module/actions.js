/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */

import Vue from 'vue'


import {
  QUERY_CUSTOMER_INFO,
} from './mutationTypes';

const actions = {
  /**
   * 取得客戶資訊
   */
  queryCustomer({ commit }) {
    Vue.prototype.$axios({
      url:'data/customer.json',
      params:{}
    }).then((resp) => { // 回應正常
      const respData = resp.data;
      commit(QUERY_CUSTOMER_INFO, {
        data: respData,
      });
    }).catch((error) => { // 異常處理
      window.console.error(error);
    });
  },
};

export default actions;
