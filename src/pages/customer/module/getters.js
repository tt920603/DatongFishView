import store from '@/store'

const getters = {
  getCustomerData(state) { // 取得所有客戶資訊
    let customers = Object.assign({}, state.customerData);
    return customers.data;
  },
  getCustomerDataBySids: () => (sids) => { // 取得客戶資訊
    let customerData = store.getters['customerModule/getCustomerData']
    let targetCustomerData = []
    //找出目標客戶
    if(customerData != null && sids){
      customerData.forEach(function (customerDataItem) {
        sids.forEach(function (sid) {
          if(customerDataItem.sid === sid) targetCustomerData.push(customerDataItem);
        });
      });
    }
    return targetCustomerData;
  },
};

export default getters;
