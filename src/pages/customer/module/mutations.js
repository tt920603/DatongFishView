import {
  QUERY_CUSTOMER_INFO,
} from './mutationTypes';

const mutations = {
  [QUERY_CUSTOMER_INFO](state, payload) { // 取得客戶資料
    state.customerData = payload;
  },
};

export default mutations;
