const getters = {
  getCompanyGoalData(state) { // 取得查詢結算訂單客戶的參數
    let companyGoalData = Object.assign({}, state.companyGoalData);
    return companyGoalData;
  },
  getBannerData(state) { // 取得查詢結算訂單客戶的參數
    let bannerData = Object.assign({}, state.bannerData);
    return bannerData;
  },
};

export default getters;
