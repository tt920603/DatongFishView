/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */

import Vue from 'vue'



import {
  QUERY_PRODUCT_INFO,
} from './mutationTypes';

const actions = {
  /**
   * 取得產品資訊
   */
  queryProduct({ commit }) {
    Vue.prototype.$axios({
      url:'data/product.json',
      params:{}
    }).then((resp) => { // 回應正常
      const respData = resp.data;
      commit(QUERY_PRODUCT_INFO, {
        data: respData,
      });
    }).catch((error) => { // 異常處理
      window.console.error(error);
    });
  },
};

export default actions;
