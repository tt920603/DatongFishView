import store from '@/store'

const getters = {
  getProductData(state) { // 取得所有產品資訊
    let products = Object.assign({}, state.productData);
    return products.data;
  },
  getRecommendProductData() { // 取得推薦的資訊
    let productData = store.getters['productModule/getProductData']
    let recommendProductData = []
    //找出推薦產品
    productData.forEach(function (productDataItem) {
      if(productDataItem.isRecommend === true) recommendProductData.push(productDataItem);
    });
    return recommendProductData;
  },
  getProductDataBySid: () => (sid) => { // 取得產品資訊
    let productData = store.getters['productModule/getProductData']
    let targetProductData = {}
    //找出目標產品
    productData.forEach(function (productDataItem) {
      if(productDataItem.sid === sid) targetProductData = productDataItem; 
    });
    return targetProductData;
  },
  getProductDataBySids: () => (sids) => { // 依據sid取得產品資訊
    let productData = store.getters['productModule/getProductData']
    let targetProductData = []
    //找出目標產品
    if(sids){
      productData.forEach(function (productDataItem) {
        sids.forEach(function (sid) {
          if(productDataItem.sid === sid) targetProductData.push(productDataItem);
        });
      });
    }
    return targetProductData;
  },
  getProductDataByName: () => (name) => { // 依據name取得產品資訊
    let productData = store.getters['productModule/getProductData']
    let targetProductData = {}
    //找出目標客戶
    if(name){
      productData.forEach(function (productDataItem) {
        if(productDataItem.title === name) targetProductData = productDataItem;
      });
    }
    return targetProductData;
  },
};

export default getters;
