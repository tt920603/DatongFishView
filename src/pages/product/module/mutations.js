import {
  QUERY_PRODUCT_INFO,
} from './mutationTypes';

const mutations = {
  [QUERY_PRODUCT_INFO](state, payload) { // 取得產品資料
    state.productData = payload;
  },
};

export default mutations;
