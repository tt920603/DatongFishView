const plugin = {
    install(Vue) {
      const $vm = Vue;
      const doList = [];
      $vm.prototype.appearObj = function (anime, target) {
        if(doList.indexOf(target) <= -1){
          doList.push(target);
          anime({
            targets: target,
            duration: 1800,
            translateX: [-1600, 0],
            complete: function(anim) {
              window.console.info('anim ', anim)

              var index = doList.indexOf(target);
              if (index !== -1) doList.splice(index, 1);
            }
        });

        }
      }
    },
  };
  
  export default plugin;
  