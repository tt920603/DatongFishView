
import homePage from '@/pages/home/views/homePage.vue'
import productDetailPage from '@/pages/product/views/productDetailPage.vue'
import productRecommendPage from '@/pages/product/views/productRecommendPage.vue'
import aboutPage from '@/pages/about/views/aboutPage.vue'

let routes = [
    {path:'/homePage',component:homePage},
    {path:'/productDetailPage',component:productDetailPage},
    {path:'/productRecommendPage',component:productRecommendPage},
    {path:'/aboutPage',component:aboutPage},
    {path:'',redirect:'/home'},
    {path:'*',component:homePage}
]

export default {
    routes:routes,
}