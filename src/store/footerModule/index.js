import actions from './actions'
import mutations from './mutations'
import getters from './getters'
import state from './state'

const footerModule = {
    namespaced: true,
    state,
    actions,
    mutations,
    getters,
};
  
export default footerModule;
