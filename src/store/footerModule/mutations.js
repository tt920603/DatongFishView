//mutations進行含資料存取
import * as types from './types';
const mutations = {
  [types.SHOWFOOTER]:(state)=>{
    state.footerIsShow = true;
  },
  [types.HIDEFOOTER]:(state)=>{
    state.footerIsShow = false;
  },
};

export default mutations;