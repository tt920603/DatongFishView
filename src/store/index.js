import Vue from 'vue';
import Vuex from 'vuex';

import footerModule from '@/store/footerModule';
import homeModule from '@/pages/home/module';
import productModule from '@/pages/product/module';
import customerModule from '@/pages/customer/module';

Vue.use(Vuex);

import axiosPlugin from '@/plugins/axios';
Vue.use(axiosPlugin);

//控管以下所有modules
export default new Vuex.Store({
  modules: {
    footerModule: footerModule,
    homeModule: homeModule,
    productModule: productModule,
    customerModule: customerModule,
  },
});
