module.exports = {
    outputDir: "dist",
    assetsDir: "static",
    // assetsDir: "./",
    publicPath: "./",
    devServer: {
    port: 9000,
    open: true,
    https: false
    }
};